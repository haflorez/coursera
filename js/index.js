$(function () {
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({ interval: 2000 });
  $('#contactoboton').on('show.bs.modal', function () {
    $('#contactoboton').removeClass('btn-outline-succes');
    $('#contactoboton').addClass('btn-primary')
  });
  $('#contactoboton').on('hide.bs.modal', function () {
    console.log('esconder')
   });
  $('#contactoboton').on('hidden.bs.modal', function () {
    console.log("escondiendo")
  });
})